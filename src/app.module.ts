import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from 'auth/auth.module';
import { getDataSourceOptions } from 'data-source';
import { UserModule } from 'user/user.module';
import { MacroLevelModule } from './macro-level/macro-level.module';
import { PlanVolsModule } from './plan-vols/plan-vols.module';
import { QualityReviewModule } from './quality-review/quality-review.module';
import { DriveTestModule } from './drive-test/drive-test.module';
import { LocationModule } from './location/location.module';
import { TowerModule } from './tower/tower.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { SuggestReviewModule } from 'suggest-review/suggest-review.module';

@Module({
  imports: [  
    TypeOrmModule.forRootAsync({
      imports: [],
      useFactory: getDataSourceOptions,
      inject: [],
    }),
    UserModule,
    AuthModule,
    MacroLevelModule,
    PlanVolsModule,
    QualityReviewModule,
    DriveTestModule,
    LocationModule,
    TowerModule,
    SuggestReviewModule
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}

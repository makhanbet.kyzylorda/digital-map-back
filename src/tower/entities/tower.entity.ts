import { ProviderTechnologies } from 'location/entities/location.entity';
import { Column, Entity, Point, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Tower {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  address: string;

  @Column()
  city: string;

  @Column({ type: 'point' })
  marker: Point;

  @Column({ type: 'jsonb' })
  operators: Operator[];
}

export type Operator = {
  cellular: ProviderTechnologies;
  owner: OperatorOwner;
};

export type OperatorOwner = 'mobile_telecom' | 'kcell' | 'kartel' | 'tele2';

import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { TowerService } from './tower.service';
import { CreateTowerDto } from './dto/create-tower.dto';
import { UpdateTowerDto } from './dto/update-tower.dto';
import { ApiTags } from '@nestjs/swagger';
import { FilterTowerDto } from './dto/filter-tower.dto';
import { ApiPaginatedResponse } from 'pagination/decorators/api-paginated-response.decorator';
import { Tower } from 'tower/entities/tower.entity';
import { PageOptionsDto } from 'pagination/dtos/page-options.dto';
import { PageDto } from 'pagination/dtos/page.dto';

@ApiTags('Towers')
@Controller('tower')
export class TowerController {
  constructor(private readonly towerService: TowerService) {}

  @Post()
  create(@Body() createTowerDto: CreateTowerDto) {
    return this.towerService.create(createTowerDto);
  }

  @Get()
  findAll(@Query() query: FilterTowerDto) {
    return this.towerService.findAll(query);
  }

  @Get('/paginated')
  @ApiPaginatedResponse(Tower)
  findAllPaginated(@Query() pageOptionsDto: PageOptionsDto): Promise<PageDto<Tower>>  {
    return this.towerService.findAllPaginated(pageOptionsDto);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateTowerDto: UpdateTowerDto) {
    return this.towerService.update(+id, updateTowerDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.towerService.remove(+id);
  }
}

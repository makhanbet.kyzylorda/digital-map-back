import { Injectable } from '@nestjs/common';
import { CreateTowerDto } from './dto/create-tower.dto';
import { UpdateTowerDto } from './dto/update-tower.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Tower } from './entities/tower.entity';
import { FindOptionsWhere, Repository } from 'typeorm';
import { FilterTowerDto } from './dto/filter-tower.dto';
import { PageOptionsDto } from 'pagination/dtos/page-options.dto';
import { PageDto } from 'pagination/dtos/page.dto';
import { PageMetaDto } from 'pagination/dtos/page-meta.dto';

@Injectable()
export class TowerService {
  constructor(
    @InjectRepository(Tower)
    private readonly towerRepo: Repository<Tower>,
  ) {}

  async create(createTowerDto: CreateTowerDto) {
    const tower = await this.towerRepo.create(createTowerDto);
    const saved = await this.towerRepo.save(tower);
    const find = await this.towerRepo.find({ where: { id: saved.id } });
    return find;
  }

  async findAll({ owner }: FilterTowerDto) {
    const queryBuilder = await this.towerRepo.createQueryBuilder('tower');
    console.log(queryBuilder);

    if (owner) {
      queryBuilder.andWhere(
        `tower.id IN (SELECT  t.id FROM "tower" t, json_array_elements(operators::json) d where d ->> 'owner' = '${owner}')`,
      );
    }
    const towers = await queryBuilder.execute();
    return towers;
  }

  public async findAllPaginated(
    pageOptionsDto: PageOptionsDto,
  ): Promise<PageDto<Tower>> {
    const queryBuilder = this.towerRepo.createQueryBuilder("t");

    queryBuilder
      .orderBy("t.id", pageOptionsDto.order)
      .skip(pageOptionsDto.skip)
      .take(pageOptionsDto.take);

    const itemCount = await queryBuilder.getCount();
    const { entities } = await queryBuilder.getRawAndEntities();

    const pageMetaDto = new PageMetaDto({ itemCount, pageOptionsDto });

    return new PageDto(entities, pageMetaDto);
  }

  async update(id: number, updateTowerDto: UpdateTowerDto) {
    const tower = await this.towerRepo.findOne({ where: { id } });
    await this.towerRepo.save({ ...tower, ...updateTowerDto });
    return this.towerRepo.findOne({ where: { id } });
  }

  async remove(id: number) {
    return this.towerRepo.remove(
      await this.towerRepo.findOneOrFail({ where: { id } }),
    );
  }
}

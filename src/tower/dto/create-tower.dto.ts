import { IsJSON, IsString } from 'class-validator';
import { Operator } from 'tower/entities/tower.entity';
import { Point } from 'typeorm';

export class CreateTowerDto {
  @IsString()
  address: string;

  @IsString()
  city: string;

  @IsString()
  marker: Point;

  @IsJSON()
  operators: Operator[];
}

import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';
import { OperatorOwner } from 'tower/entities/tower.entity';

export class FilterTowerDto {
  @ApiPropertyOptional()
  @IsOptional()
  owner: OperatorOwner;
}

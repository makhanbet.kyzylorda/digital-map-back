import { IsOptional } from 'class-validator';

export class FilterPlanVolsDto {
  @IsOptional()
  name?: string | undefined;

  @IsOptional()
  year?: number | undefined;
}
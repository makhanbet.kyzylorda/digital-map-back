import { PartialType } from '@nestjs/swagger';
import { CreatePlanVolDto } from './create-plan-vol.dto';

export class UpdatePlanVolDto extends PartialType(CreatePlanVolDto) {}

import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { PlanVolsService } from './plan-vols.service';
import { CreatePlanVolDto } from './dto/create-plan-vol.dto';
import { UpdatePlanVolDto } from './dto/update-plan-vol.dto';
import { ApiTags } from '@nestjs/swagger';
import { FilterPlanVolsDto } from './dto/get-plan-vols.dto';
import { ApiPaginatedResponse } from 'pagination/decorators/api-paginated-response.decorator';
import { PlanVols } from 'plan-vols/entities/plan-vols.entity';
import { PageOptionsDto } from 'pagination/dtos/page-options.dto';
import { PageDto } from 'pagination/dtos/page.dto';

@ApiTags('Plan vols')
@Controller('plan-vols')
export class PlanVolsController {
  constructor(private readonly planVolsService: PlanVolsService) {}

  @Post()
  create(@Body() createPlanVolDto: CreatePlanVolDto) {
    return this.planVolsService.create(createPlanVolDto);
  }

  @Get()
  findAll(
    @Query() query: FilterPlanVolsDto
  ) {
    return this.planVolsService.findAll(query);
  }

  @Get('/paginated')
  @ApiPaginatedResponse(PlanVols)
  findAllPaginated(@Query() pageOptionsDto: PageOptionsDto): Promise<PageDto<PlanVols>>  {
    return this.planVolsService.findAllPaginated(pageOptionsDto);
  }


  @Patch(':id')
  update(@Param('id') id: string, @Body() updatePlanVolDto: UpdatePlanVolDto) {
    return this.planVolsService.update(+id, updatePlanVolDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.planVolsService.remove(+id);
  }
}

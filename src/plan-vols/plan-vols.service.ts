import { Injectable } from '@nestjs/common';
import { CreatePlanVolDto } from './dto/create-plan-vol.dto';
import { UpdatePlanVolDto } from './dto/update-plan-vol.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { PlanVols } from './entities/plan-vols.entity';
import { FindOptionsWhere, Like, Repository } from 'typeorm';
import { FilterPlanVolsDto } from './dto/get-plan-vols.dto';
import { PageOptionsDto } from 'pagination/dtos/page-options.dto';
import { PageDto } from 'pagination/dtos/page.dto';
import { PageMetaDto } from 'pagination/dtos/page-meta.dto';

@Injectable()
export class PlanVolsService {
  constructor(
    @InjectRepository(PlanVols)
    private readonly planVolsRepo: Repository<PlanVols>,
  ) {}

  async create(createPlanVolDto: CreatePlanVolDto) {
    const planVols = await this.planVolsRepo.create(createPlanVolDto);
    const save = await this.planVolsRepo.save(planVols);
    return this.planVolsRepo.findOne({ where: { id: save.id } });
  }

  async findAll({ year, name }: FilterPlanVolsDto) {
    const where: FindOptionsWhere<PlanVols> = {};
    if (name) where.name = Like(`%${name}%`);
    if (year) where.year = year;

    const planVols = await this.planVolsRepo.find({ where });
    return planVols;
  }

  public async findAllPaginated(
    pageOptionsDto: PageOptionsDto,
  ): Promise<PageDto<PlanVols>> {
    const queryBuilder = this.planVolsRepo.createQueryBuilder("pv");

    queryBuilder
      .orderBy("pv.id", pageOptionsDto.order)
      .skip(pageOptionsDto.skip)
      .take(pageOptionsDto.take);

    const itemCount = await queryBuilder.getCount();
    const { entities } = await queryBuilder.getRawAndEntities();

    const pageMetaDto = new PageMetaDto({ itemCount, pageOptionsDto });

    return new PageDto(entities, pageMetaDto);
  }

  async update(id: number, updatePlanVolDto: UpdatePlanVolDto) {
    const planVols = await this.planVolsRepo.findOne({ where: { id } });
    await this.planVolsRepo.save({ ...planVols, ...updatePlanVolDto });
    return this.planVolsRepo.findOne({ where: { id } });
  }

  async remove(id: number) {
    return this.planVolsRepo.remove(
      await this.planVolsRepo.findOneOrFail({ where: { id } }),
    );
  }
}

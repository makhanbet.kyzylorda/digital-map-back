import { Column, Entity, Geography, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'plan_vols' })
export class PlanVols {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({ nullable: true })
  population: number;

  @Column({ nullable: true })
  year: number;

  @Column({ type: 'geography', spatialFeatureType: 'Point', srid: 4326 })
  geog: Geography;

  @Column()
  technologies: string;

  @Column({ type: 'jsonb' })
  details: string;
}

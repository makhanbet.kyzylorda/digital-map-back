import { Module } from '@nestjs/common';
import { PlanVolsService } from './plan-vols.service';
import { PlanVolsController } from './plan-vols.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PlanVols } from './entities/plan-vols.entity';

@Module({
  imports: [TypeOrmModule.forFeature([PlanVols])],
  controllers: [PlanVolsController],
  providers: [PlanVolsService],
})
export class PlanVolsModule {}

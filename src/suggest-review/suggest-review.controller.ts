import {
  Controller,
  Get,
  Post,
  Body,
  UseInterceptors,
  Delete,
  Param,
  Query,
} from '@nestjs/common';
import { SuggestReviewService } from './suggest-review.service';
import { CreateSuggestReviewDto } from './dto/create-suggest-review.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiTags } from '@nestjs/swagger';
import { SuggestReview } from './entities/suggest-review.entity';
import { ApiPaginatedResponse } from 'pagination/decorators/api-paginated-response.decorator';
import { PageDto } from 'pagination/dtos/page.dto';
import { PageOptionsDto } from 'pagination/dtos/page-options.dto';

@ApiTags('Suggest review')
@Controller('suggest-review')
export class SuggestReviewController {
  constructor(private readonly suggestReviewService: SuggestReviewService) {}

  @Post()
  @UseInterceptors(FileInterceptor('file'))
  async create(@Body() createSuggestReviewDto: CreateSuggestReviewDto) {
    return this.suggestReviewService.create(createSuggestReviewDto);
  }

  @Get()
  @ApiPaginatedResponse(SuggestReview)
  findAll(
    @Query() pageOptionsDto: PageOptionsDto,
  ): Promise<PageDto<SuggestReview>> {
    return this.suggestReviewService.findAll(pageOptionsDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.suggestReviewService.remove(+id);
  }
}

import { PartialType } from '@nestjs/swagger';
import { CreateSuggestReviewDto } from './create-suggest-review.dto';

export class UpdateSuggestReviewDto extends PartialType(CreateSuggestReviewDto) {}

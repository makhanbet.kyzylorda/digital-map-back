import { Module } from '@nestjs/common';
import { SuggestReviewService } from './suggest-review.service';
import { SuggestReviewController } from './suggest-review.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SuggestReview } from './entities/suggest-review.entity';

@Module({
  imports: [TypeOrmModule.forFeature([SuggestReview])],
  controllers: [SuggestReviewController],
  providers: [SuggestReviewService],
})
export class SuggestReviewModule {}

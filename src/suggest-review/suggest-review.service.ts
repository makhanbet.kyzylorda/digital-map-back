import { Injectable } from '@nestjs/common';
import { CreateSuggestReviewDto } from './dto/create-suggest-review.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { SuggestReview } from './entities/suggest-review.entity';
import { PageOptionsDto } from 'pagination/dtos/page-options.dto';
import { PageDto } from 'pagination/dtos/page.dto';
import { PageMetaDto } from 'pagination/dtos/page-meta.dto';

@Injectable()
export class SuggestReviewService {
  constructor(
    @InjectRepository(SuggestReview)
    private readonly suggestReviewRepository: Repository<SuggestReview>,
  ) {}

  async create(createSuggestReviewDto: CreateSuggestReviewDto) {
    const review = await this.suggestReviewRepository.create({
      ...createSuggestReviewDto,
    });

    const reviewSaved = await this.suggestReviewRepository.save(review);
    return await this.suggestReviewRepository.findOne({
      where: { id: reviewSaved.id },
    });
  }

  public async findAll(
    pageOptionsDto: PageOptionsDto,
  ): Promise<PageDto<SuggestReview>> {
    const queryBuilder = this.suggestReviewRepository.createQueryBuilder('qr');

    queryBuilder
      .orderBy('qr.createdAt', pageOptionsDto.order)
      .skip(pageOptionsDto.skip)
      .take(pageOptionsDto.take);

    const itemCount = await queryBuilder.getCount();
    const { entities } = await queryBuilder.getRawAndEntities();

    const pageMetaDto = new PageMetaDto({ itemCount, pageOptionsDto });

    return new PageDto(entities, pageMetaDto);
  }

  async remove(id: number) {
    return this.suggestReviewRepository.softDelete({ id });
  }
}

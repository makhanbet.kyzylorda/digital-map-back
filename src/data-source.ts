import { DataSourceOptions } from 'typeorm';
import { join } from 'path';

export const getDataSourceOptions = (): DataSourceOptions => ({
  type: 'postgres',
  host: '193.148.60.86',
  port: 1,
  username: 'postgres',
  password: 'password',
  database: 'map',
  entities: [join(__dirname, './**/*.entity{.ts,.js}')],
  migrations: [join(__dirname, './migrations/*{.ts,.js}')],
  synchronize: true,
  dropSchema: false,
  logging: false,
});

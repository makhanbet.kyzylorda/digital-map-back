import { Injectable } from '@nestjs/common';
import { CreateMacroLevelDto } from './dto/create-macro-level.dto';
import { UpdateMacroLevelDto } from './dto/update-macro-level.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { MacroLevel } from './entities/macro-level.entity';
import { FindOptionsWhere, Repository } from 'typeorm';
import { FilterMacroLevelDto } from './dto/filter-macro-level.dto';
import { PageOptionsDto } from 'pagination/dtos/page-options.dto';
import { PageDto } from 'pagination/dtos/page.dto';
import { PageMetaDto } from 'pagination/dtos/page-meta.dto';

@Injectable()
export class MacroLevelService {
  constructor(
    @InjectRepository(MacroLevel)
    private readonly macroLevelRepository: Repository<MacroLevel>,
  ) {}

  async create(createMacroLevelDto: CreateMacroLevelDto) {
    const macroLevel =
      await this.macroLevelRepository.create(createMacroLevelDto);
    const save = await this.macroLevelRepository.save(macroLevel);
    return this.macroLevelRepository.findOne({ where: { id: save.id } });
  }

  async findAll({ level, threshold, city }: FilterMacroLevelDto) {
    const where: FindOptionsWhere<FilterMacroLevelDto> = {};
    if (level) where.level = level;
    if (threshold) where.threshold = threshold;
    if (city) where.city = city;

    const macrolLevel = await this.macroLevelRepository.find({ where });
    return macrolLevel;
  }

  public async findAllPaginated(
    pageOptionsDto: PageOptionsDto,
  ): Promise<PageDto<MacroLevel>> {
    const queryBuilder = this.macroLevelRepository.createQueryBuilder("ml");

    queryBuilder
      .orderBy("ml.id", pageOptionsDto.order)
      .skip(pageOptionsDto.skip)
      .take(pageOptionsDto.take);

    const itemCount = await queryBuilder.getCount();
    const { entities } = await queryBuilder.getRawAndEntities();

    const pageMetaDto = new PageMetaDto({ itemCount, pageOptionsDto });

    return new PageDto(entities, pageMetaDto);
  }

  async update(id: number, updateMacroLevelDto: UpdateMacroLevelDto) {
    const macroLevel = await this.macroLevelRepository.findOne({
      where: { id },
    });
    await this.macroLevelRepository.save({
      ...macroLevel,
      ...updateMacroLevelDto,
    });
    return this.macroLevelRepository.findOne({ where: { id } });
  }

  async remove(id: number) {
    return this.macroLevelRepository.remove(
      await this.macroLevelRepository.findOneOrFail({ where: { id } }),
    );
  }
}

import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { MacroLevelService } from './macro-level.service';
import { CreateMacroLevelDto } from './dto/create-macro-level.dto';
import { UpdateMacroLevelDto } from './dto/update-macro-level.dto';
import { ApiTags } from '@nestjs/swagger';
import { FilterMacroLevelDto } from './dto/filter-macro-level.dto';
import { ApiPaginatedResponse } from 'pagination/decorators/api-paginated-response.decorator';
import { MacroLevel } from 'macro-level/entities/macro-level.entity';
import { PageOptionsDto } from 'pagination/dtos/page-options.dto';
import { PageDto } from 'pagination/dtos/page.dto';

@ApiTags('Macro level')
@Controller('macro-level')
export class MacroLevelController {
  constructor(private readonly macroLevelService: MacroLevelService) {}

  @Post()
  create(@Body() createMacroLevelDto: CreateMacroLevelDto) {
    return this.macroLevelService.create(createMacroLevelDto);
  }

  @Get()
  findAll(
    @Query() query: FilterMacroLevelDto
  ) {
    return this.macroLevelService.findAll(query);
  }

  @Get('/paginated')
  @ApiPaginatedResponse(MacroLevel)
  findAllPaginated(@Query() pageOptionsDto: PageOptionsDto): Promise<PageDto<MacroLevel>>  {
    return this.macroLevelService.findAllPaginated(pageOptionsDto);
  }


  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateMacroLevelDto: UpdateMacroLevelDto,
  ) {
    return this.macroLevelService.update(+id, updateMacroLevelDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.macroLevelService.remove(+id);
  }
}

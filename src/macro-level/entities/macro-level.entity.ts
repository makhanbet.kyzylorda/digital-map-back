import { Column, Entity, Geography, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ synchronize: false })
export class MacroLevel {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  level: number;

  @Column()
  city: string;

  @Column()
  legend: string;

  @Column()
  threshold: number;

  @Column()
  color: string;

  @Column({ nullable: true })
  name?: string;

  @Column({
    type: 'geography',
    spatialFeatureType: 'MultiPolygonZ',
    srid: 4326,
  })
  coordinates: Geography;
}

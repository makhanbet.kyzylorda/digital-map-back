import { IsJSON, IsNumber, IsOptional, IsString } from "class-validator";
import { Geometry } from "typeorm";

export class CreateMacroLevelDto {
  @IsNumber()
  level: 10 | 50 | 100;

  @IsString()
  city: string;

  @IsString()
  legend: string;

  @IsNumber()
  threshold: number;

  @IsString()
  color: string;

  @IsString()
  @IsOptional()
  name?: string;

  @IsJSON()
  coordinates: Geometry;
}

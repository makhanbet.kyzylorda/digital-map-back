import { IsOptional } from 'class-validator';

export class FilterMacroLevelDto {
  @IsOptional()
  level?: number | undefined;

  @IsOptional()
  city?: string | undefined;

  @IsOptional()
  threshold?: number | undefined;
}

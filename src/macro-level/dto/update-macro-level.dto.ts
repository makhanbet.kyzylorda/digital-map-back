import { PartialType } from '@nestjs/swagger';
import { CreateMacroLevelDto } from './create-macro-level.dto';

export class UpdateMacroLevelDto extends PartialType(CreateMacroLevelDto) {}

import { Module } from '@nestjs/common';
import { MacroLevelService } from './macro-level.service';
import { MacroLevelController } from './macro-level.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MacroLevel } from './entities/macro-level.entity';

@Module({
  imports: [TypeOrmModule.forFeature([MacroLevel])],
  controllers: [MacroLevelController],
  providers: [MacroLevelService],
})
export class MacroLevelModule {}

import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Post,
  Request,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ApiBody, ApiTags } from '@nestjs/swagger';

import { AuthUser } from '../user/decorator/user.decorator';
import { User, UserGroups } from '../user/entities/user.entity';
import { AuthService } from './auth.service';
import { SignUp } from './dto/sign-up.dto';
import { JWTAuthGuard } from './guards/jwt-auth.guard';
import { LocalAuthGuard } from './guards/local-auth.guard';
import { SessionAuthGuard } from './guards/session-auth.guard';
import { TokenInterceptor } from './interceptors/token.interceptor';
import { UpdateResult } from 'typeorm';
import { UserService } from 'user/user.service';
import { ResetPasswordDto } from 'auth/dto/reset-password.dto';
import { AuthGuard } from '@nestjs/passport';
import { GroupsGuard } from './guards/groups.guard';
import { Groups } from 'user/decorator/group.decorator';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly userService: UserService,
  ) {}

  @Post('register')
  @HttpCode(HttpStatus.CREATED)
  @UseInterceptors(TokenInterceptor)
  // @UseGuards(JWTAuthGuard, SessionAuthGuard, GroupsGuard)
  // @Groups(UserGroups.ADMIN)
  register(@Body() signUp: SignUp): Promise<User> {
    return this.authService.register(signUp);
  }

  @Post('login')
  @UseGuards(LocalAuthGuard)
  @HttpCode(HttpStatus.OK)
  @UseInterceptors(TokenInterceptor)
  @ApiBody({
    description: 'Sign in data',
    schema: {
      type: 'object',
      properties: {
        email: { type: 'string' },
        password: { type: 'string' },
      },
    },
  })
  async login(@AuthUser() user: User): Promise<User> {
    return user;
  }

  @Get('/me')
  @UseGuards(JWTAuthGuard)
  me(@Request() req): User {
    console.log(req.user);

    return req.user;
  }

  @Post('/reset-password')
  @UseGuards(JWTAuthGuard, SessionAuthGuard)
  async resetPassword(
    @Body() resetPasswordDto: ResetPasswordDto,
  ): Promise<Partial<User>> {
    return await this.userService.updateUser(resetPasswordDto.id, {
      password: resetPasswordDto.password,
    });
  }
}

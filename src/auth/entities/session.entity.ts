import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity()
export class Session {
  @PrimaryColumn({
    type: 'varchar',
    nullable: false,
  })
  sid: number;

  @Column({
    type: 'varchar',
    nullable: false,
  })
  sess: any;

  @Column({
    nullable: false,
  })
  expire: Date;
}

import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';

@Injectable()
export class GroupsGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) {
  }

  canActivate(context: ExecutionContext): boolean {
    const groups = this.reflector.get<string[]>('userGroup', context.getHandler());

    if (!groups) {
      return false;
    }

    const { user } = context.switchToHttp().getRequest();

    return groups.some((group) => {
      return group === user.userGroup;
    });
  }
}

import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

import { User } from '../user/entities/user.entity';
import { SignUp } from './dto/sign-up.dto';
import { JwtPayload } from './interfaces/jwt-payload.interface';
import { UserService } from 'user/user.service';

@Injectable()
export class AuthService {
  constructor(
    private readonly jwtService: JwtService,
    private readonly userService: UserService,
  ) {}

  async register(signUp: SignUp): Promise<User> {
    const user = await this.userService.create(signUp);
    delete user.password;
    return user;
  }

  async login(email: string, password: string): Promise<User> {
    let user: User;
    try {
      user = await this.userService.findOne({
        where: { email, isActive: true },
      });
    } catch (err) {
      throw new UnauthorizedException(`Incorrect credentials`);
    }

    if (!(await user.checkPassword(password))) {
      throw new UnauthorizedException(`Incorrect credentials`);
    }

    return user;
  }

  async verifyPayload(payload: JwtPayload): Promise<User> {
    let user: User;

    try {
      user = await this.userService.findOne({
        where: { email: payload.sub },
      });
    } catch (error) {
      throw new UnauthorizedException(
        `There isn't any user with email: ${payload.sub}`,
      );
    }
    delete user.password;
    console.log(user);
    
    return user;
  }

  signToken(user: User): string {
    const payload = {
      sub: user.email,
    };

    return this.jwtService.sign(payload);
  }
}

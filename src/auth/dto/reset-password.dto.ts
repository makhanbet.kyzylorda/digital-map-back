import { SignUp } from './sign-up.dto';
import { IsDefined, IsNotEmpty, IsNumber, MinLength } from 'class-validator';

export class ResetPasswordDto {
  @IsDefined()
  @IsNumber()
  readonly id: number;

  @IsDefined()
  @IsNotEmpty()
  @MinLength(8)
  readonly password: string;
}

import {
  IsDefined,
  IsEmail,
  IsEnum,
  IsNotEmpty,
  IsNumberString,
  IsOptional,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { UserGroups } from 'user/entities/user.entity';
import { isUserAlreadyExist } from 'user/decorator/is-user-already-exist.decorator';

export class SignUp {
  @ApiProperty()
  @IsDefined()
  @IsNotEmpty()
  readonly name: string;

  @ApiProperty()
  @IsEmail()
  @isUserAlreadyExist({ column: 'email' })
  readonly email: string;

  @ApiProperty()
  @IsNumberString()
  @MinLength(12)
  @MaxLength(12)
  @isUserAlreadyExist({ column: 'iin' })
  readonly iin: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  readonly organization: string;

  @ApiProperty()
  @IsDefined()
  @IsNotEmpty()
  @MinLength(8)
  readonly password: string;

  @IsEnum(UserGroups)
  @IsOptional()
  readonly userGroup: UserGroups;
}

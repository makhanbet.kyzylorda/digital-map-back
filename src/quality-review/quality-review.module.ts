import { Module } from '@nestjs/common';
import { QualityReviewService } from './quality-review.service';
import { QualityReviewController } from './quality-review.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { QualityReview } from './entities/quality-review.entity';

@Module({
  imports: [TypeOrmModule.forFeature([QualityReview])],
  controllers: [QualityReviewController],
  providers: [QualityReviewService],
})
export class QualityReviewModule {}

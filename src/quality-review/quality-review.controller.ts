import {
  Controller,
  Get,
  Post,
  Body,
  UseInterceptors,
  UploadedFile,
  ParseFilePipe,
  MaxFileSizeValidator,
  FileTypeValidator,
  Delete,
  Param,
  Query,
} from '@nestjs/common';
import { QualityReviewService } from './quality-review.service';
import { CreateQualityReviewDto } from './dto/create-quality-review.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiTags } from '@nestjs/swagger';
import { QualityReview } from './entities/quality-review.entity';
import { ApiPaginatedResponse } from 'pagination/decorators/api-paginated-response.decorator';
import { PageDto } from 'pagination/dtos/page.dto';
import { PageOptionsDto } from 'pagination/dtos/page-options.dto';

@ApiTags('Quality review')
@Controller('quality-review')
export class QualityReviewController {
  constructor(private readonly qualityReviewService: QualityReviewService) {}

  @Post()
  @UseInterceptors(FileInterceptor('file'))
  async create(
    @Body() createQualityReviewDto: CreateQualityReviewDto,

    @UploadedFile(
      new ParseFilePipe({
        validators: [
          new MaxFileSizeValidator({ maxSize: 15000000 }),
          new FileTypeValidator({ fileType: '.(pdf|jpeg|jpg)' }),
        ],
        fileIsRequired: false,
      }),
    )
    file?: Express.Multer.File,
  ) {
    return this.qualityReviewService.create(createQualityReviewDto, file);
  }

  @Get()
  @ApiPaginatedResponse(QualityReview)
  findAll(@Query() pageOptionsDto: PageOptionsDto): Promise<PageDto<QualityReview>>  {
    return this.qualityReviewService.findAll(pageOptionsDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.qualityReviewService.remove(+id);
  }
}

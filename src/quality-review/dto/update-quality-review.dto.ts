import { PartialType } from '@nestjs/swagger';
import { CreateQualityReviewDto } from './create-quality-review.dto';

export class UpdateQualityReviewDto extends PartialType(CreateQualityReviewDto) {}

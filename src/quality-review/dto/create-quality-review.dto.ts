import {
  IsEmail,
  IsNotEmpty,
  IsNumberString,
  IsOptional,
  IsString,
} from 'class-validator';
import { Point } from 'typeorm';

export class CreateQualityReviewDto {
  @IsString()
  @IsOptional()
  phoneNumber: string;

  @IsEmail()
  @IsOptional()
  email: string;

  @IsString()
  @IsOptional()
  name: string;

  @IsString()
  @IsNotEmpty()
  operator: string;

  @IsNumberString()
  // @Max(5)
  // @Min(1)
  // @Type(() => Number)
  rating: number;

  @IsString()
  @IsOptional()
  marker: Point;

  @IsString()
  @IsOptional()
  address: string;
}

import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  Point,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class QualityReview {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true })
  phoneNumber?: string;

  @Column({ nullable: true })
  email?: string;

  @Column({ nullable: true })
  name: string;

  @Column({ nullable: true })
  filePath?: string;

  @Column()
  operator: string;

  @Column({ default: 0 })
  rating: number;

  @Column({ type: 'point', nullable: true })
  marker?: Point;

  @Column({ nullable: true })
  address?: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;
}

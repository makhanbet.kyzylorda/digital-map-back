import { Injectable } from '@nestjs/common';
import { CreateQualityReviewDto } from './dto/create-quality-review.dto';
import { S3 } from 'aws-sdk';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, Repository } from 'typeorm';
import { QualityReview } from './entities/quality-review.entity';
import { PageOptionsDto } from 'pagination/dtos/page-options.dto';
import { PageDto } from 'pagination/dtos/page.dto';
import { PageMetaDto } from 'pagination/dtos/page-meta.dto';

@Injectable()
export class QualityReviewService {
  s3Stream = new S3({
    accessKeyId: 'd4da23cc4ecf',
    secretAccessKey: '06cee85247b',
    endpoint: 'http://193.148.60.86:9000',
    s3ForcePathStyle: true, // needed with minio?
    signatureVersion: 'v4',
    region: 'us-east-1',
  });

  constructor(
    @InjectRepository(QualityReview)
    private readonly qualityReviewRepository: Repository<QualityReview>,
  ) { }

  async create(
    createQualityReviewDto: CreateQualityReviewDto,
    file?: Express.Multer.File,
  ) {
    const review = await this.qualityReviewRepository.create({
      ...createQualityReviewDto,
    });
    if (file) {
      const result = await this.s3Stream
        .upload({
          Bucket: 'digital-map',
          Key: file.originalname,
          Body: file.buffer,
        })
        .promise();
      review.filePath = result.Location;
    }

    const reviewSaved = await this.qualityReviewRepository.save(review);
    return await this.qualityReviewRepository.findOne({
      where: { id: reviewSaved.id },
    });
  }

  public async findAll(
    pageOptionsDto: PageOptionsDto,
  ): Promise<PageDto<QualityReview>> {
    const queryBuilder = this.qualityReviewRepository.createQueryBuilder("qr");

    queryBuilder
      .orderBy("qr.createdAt", pageOptionsDto.order)
      .skip(pageOptionsDto.skip)
      .take(pageOptionsDto.take);

    const itemCount = await queryBuilder.getCount();
    const { entities } = await queryBuilder.getRawAndEntities();

    const pageMetaDto = new PageMetaDto({ itemCount, pageOptionsDto });

    return new PageDto(entities, pageMetaDto);
  }


  async remove(id: number) {
    return this.qualityReviewRepository.softDelete(
      { id }
    );
  }
}

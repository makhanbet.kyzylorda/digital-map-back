import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  BadRequestException,
  Res,
} from '@nestjs/common';
import { DriveTestService } from './drive-test.service';
import { CreateDriveTestDto } from './dto/create-drive-test.dto';
import { UpdateDriveTestDto } from './dto/update-drive-test.dto';
import { FilterDriveDto } from './dto/filter-drive-test.dto';
import { In } from 'typeorm';
import { Response } from 'express';
import { ApiPaginatedResponse } from 'pagination/decorators/api-paginated-response.decorator';
import { DriveTest } from 'drive-test/entities/drive-test.entity';
import { PageOptionsDto } from 'pagination/dtos/page-options.dto';
import { PageDto } from 'pagination/dtos/page.dto';

@Controller('drive-test')
export class DriveTestController {
  constructor(private readonly driveTestService: DriveTestService) {}

  pathMakesSense = (z, x, y) => {
    const maxCoord = 2 ** z;
    return (
      z >= 0 && z <= 20 && x >= 0 && x < maxCoord && y >= 0 && y < maxCoord
    );
  };

  @Post()
  create(@Body() createDriveTestDto: CreateDriveTestDto) {
    return this.driveTestService.create(createDriveTestDto);
  }

  @Get()
  findAll(@Query() query: FilterDriveDto) {
    return this.driveTestService.findAll(query);
  }

  @Get('/cities')
  findCities() {
    return this.driveTestService.findCities();
  }

  @Get('/paginated')
  @ApiPaginatedResponse(DriveTest)
  findAllPaginated(@Query() pageOptionsDto: PageOptionsDto): Promise<PageDto<DriveTest>>  {
    return this.driveTestService.findAllPaginated(pageOptionsDto);
  }

  @Get('raster/:z/:x/:y')
  async findRaster(
    @Res() res: Response,
    @Param('z') zoom: number,
    @Param('x') x: number,
    @Param('y') y: number,
  ) {
    if (this.pathMakesSense(zoom, x, y)) {
      const image = await this.driveTestService.findAsRaster(zoom, x, y);

      res.set({
        'Content-Type': 'image/png',
        'Content-Length': image.length,
      });
      res.end(image);
    } else throw new BadRequestException('Incorrect path');
  }

  @Get('ids')
  fetchClusterByIds(@Query('ids') ids: number[] | number) {
    return this.driveTestService.findByExtended({
      id: In(Array.isArray(ids) ? ids : [ids]),
    });
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.driveTestService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateDriveTestDto: UpdateDriveTestDto,
  ) {
    return this.driveTestService.update(+id, updateDriveTestDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.driveTestService.remove(+id);
  }
}

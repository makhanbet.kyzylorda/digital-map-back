import {
  Column,
  CreateDateColumn,
  Entity,
  Point,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

export enum Technology {
  '2G' = '2G',
  '3G' = '3G',
  '4G' = '4G',
  '5G' = '5G',
  'FTP' = 'FTP'
}

export enum Operator {
  'Beeline' = 'Beeline',
  'Tele2' = 'Altel, Tele2.kz',
  'Kcell' = 'Kcell',
}

@Entity()
export class DriveTest {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  city: string;

  @Column({ type: 'enum', enum: Technology })
  technology: Technology;

  @Column({ type: 'enum', enum: Operator })
  operator: Operator;

  @Column({ type: 'timestamp' })
  datetime: Date;

  @Column({ type: 'point' })
  marker: Point;

  @Column({ type: 'numeric' })
  rsrp: number;

  @Column({ type:'integer', nullable: true })
  level: number

  @CreateDateColumn({ select: false })
  createdAt: Date;

  @UpdateDateColumn({ select: false })
  updatedAt: Date;
}

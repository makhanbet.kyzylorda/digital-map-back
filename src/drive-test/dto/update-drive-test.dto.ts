import { PartialType } from '@nestjs/mapped-types';
import { CreateDriveTestDto } from './create-drive-test.dto';

export class UpdateDriveTestDto extends PartialType(CreateDriveTestDto) {}

import { IsDateString, IsEnum, IsNumber, IsString } from "class-validator";
import { Operator, Technology } from "drive-test/entities/drive-test.entity";
import { Point } from "typeorm";

export class CreateDriveTestDto {
  @IsString()
  city: string;

  @IsEnum(Technology)
  technology: Technology;

  @IsEnum(Operator)
  operator: Operator;

  @IsDateString()
  datetime: Date;

  @IsString()
  marker: Point;

  @IsNumber()
  rsrp: number;
}


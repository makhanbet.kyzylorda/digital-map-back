import {
  IsDateString,
  IsEnum,
  IsNumberString,
  IsOptional,
} from 'class-validator';
import { Operator, Technology } from 'drive-test/entities/drive-test.entity';

export class FilterDriveDto {
  @IsOptional()
  minRSCP?: number | undefined;

  @IsOptional()
  maxRSCP?: number | undefined;

  @IsOptional()
  city?: string | undefined;

  @IsOptional()
  @IsDateString()
  minDatetime?: Date | undefined;

  @IsOptional()
  @IsDateString()
  maxDatetime?: Date | undefined;

  // @IsEnum(Operator)
  @IsOptional()
  operator?: Operator[] | Operator | undefined;

  @IsOptional()
  @IsEnum(Technology)
  technology?: Technology | undefined;

  @IsOptional()
  @IsNumberString()
  x1?: number;

  @IsOptional()
  @IsNumberString()
  y1?: number;

  @IsOptional()
  @IsNumberString()
  x2?: number;

  @IsOptional()
  @IsNumberString()
  y2?: number;
}

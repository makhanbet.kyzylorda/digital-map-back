import { Injectable } from '@nestjs/common';
import { CreateDriveTestDto } from './dto/create-drive-test.dto';
import { UpdateDriveTestDto } from './dto/update-drive-test.dto';
import { DriveTest } from './entities/drive-test.entity';
import { DataSource, FindOptionsWhere, Repository } from 'typeorm';
import { InjectDataSource, InjectRepository } from '@nestjs/typeorm';
import { FilterDriveDto } from './dto/filter-drive-test.dto';
import { PageOptionsDto } from 'pagination/dtos/page-options.dto';
import { PageDto } from 'pagination/dtos/page.dto';
import { QualityReview } from 'quality-review/entities/quality-review.entity';
import { PageMetaDto } from 'pagination/dtos/page-meta.dto';

@Injectable()
export class DriveTestService {
  constructor(
    @InjectRepository(DriveTest)
    private readonly driveTestRepo: Repository<DriveTest>,
    @InjectDataSource()
    private dataSource: DataSource,
  ) {}

  async create(createDriveTestDto: CreateDriveTestDto) {
    const driveTest = await this.driveTestRepo.create(createDriveTestDto);
    const save = await this.driveTestRepo.save(driveTest);
    return this.driveTestRepo.findOne({ where: { id: save.id } });
  }

  async findByExtended(where: FindOptionsWhere<DriveTest>) {
    return this.driveTestRepo.find({ where });
  }

  async findCities() {
    return this.driveTestRepo.createQueryBuilder('dt')
      .select('dt.city')
      .distinct(true)
      .getRawMany();
  }

  public async findAllPaginated(
    pageOptionsDto: PageOptionsDto,
  ): Promise<PageDto<DriveTest>> {
    const queryBuilder = this.driveTestRepo.createQueryBuilder('dt');

    queryBuilder
      .orderBy('dt.createdAt', pageOptionsDto.order)
      .skip(pageOptionsDto.skip)
      .take(pageOptionsDto.take);

    const itemCount = await queryBuilder.getCount();
    const { entities } = await queryBuilder.getRawAndEntities();

    const pageMetaDto = new PageMetaDto({ itemCount, pageOptionsDto });

    return new PageDto(entities, pageMetaDto);
  }

  async findAsRaster(zoom: number, x: number, y: number) {
    const query = `SELECT ST_AsPNG(
      ST_AsRaster(
          ST_collect(Array(
              SELECT ST_Intersection(ST_Transform(ST_GeomFromText('POINT(' || ST_y(marker::geometry) || ' ' || ST_x(marker::geometry) || ')', 4326), 3857),ST_TileEnvelope($1,$2,$3)) FROM drive_test UNION
              SELECT ST_boundary(ST_TileEnvelope($1,$2,$3))
          )
      ), 256, 256, ARRAY['8BUI', '8BUI', '8BUI'], ARRAY[100,100,100], ARRAY[0,0,0])
    );`;

    const now = Date.now();

    await this.dataSource.query(
      `SET postgis.gdal_enabled_drivers = 'ENABLE_ALL'`,
    );

    const interval = Date.now();

    const result = await this.dataSource.query(query, [zoom, x, y]);
    const img = result[0].st_aspng as Buffer;
    return img;
  }

  async findAll({
    minRSCP,
    maxRSCP,
    operator,
    city,
    technology,
    minDatetime,
    maxDatetime,
    x1,
    x2,
    y1,
    y2,
  }: FilterDriveDto) {
    // const where: FindOptionsWhere<DriveTest> = {};

    // console.log(where);

    // const tests = await this.driveTestRepo.find({
    //   where,
    //   select: { id: true, marker: { coordinates: true } },
    // });
    // console.log(tests.length);
    const subQuery = this.dataSource.createQueryBuilder();

    subQuery
      // .select([
      //   'ST_x(ST_Centroid(ST_Collect(marker::geometry))) as x',
      //   'ST_y(ST_Centroid(ST_Collect(marker::geometry))) as y',
      //   'level',
      //   'array_agg(id) as ids',
      // ])
      // .addFrom((subQuery) => {
      //   subQuery
      .select([
        'id',
        // 'ST_ClusterDBSCAN(marker::geometry,0.0001,0) OVER() as cl_id',
        'ST_x(marker::geometry) as x',
        'ST_y(marker::geometry) as y',
        'level',
        // 'marker',
      ])
      .from(DriveTest, 'drive-test');
    if (minRSCP && maxRSCP) {
      subQuery.andWhere(`rsrp >= ${minRSCP}`).andWhere(`rsrp <= ${maxRSCP}`);
    } else if (minRSCP) subQuery.andWhere(`rsrp >= ${minRSCP}`);
    else if (maxRSCP) subQuery.andWhere(`rsrp <= ${maxRSCP}`);

    if (minDatetime && maxDatetime) {
      subQuery
        .andWhere(`datetime >= '${minDatetime}'`)
        .andWhere(`datetime <= '${maxDatetime}'`);
    } else if (minDatetime) subQuery.andWhere(`datetime >= '${minDatetime}'`);
    else if (maxDatetime) subQuery.andWhere(`datetime <= '${maxDatetime}'`);

    if (operator) {
      if (Array.isArray(operator)) {
        subQuery.andWhere(
          `operator in(${operator.map((o) => `'${o}'`).join(',')})`,
        );
      } else {
        subQuery.andWhere(`operator='${operator}'`);
      }
    }
    if (technology) subQuery.andWhere(`technology='${technology}'`);
    if (city) subQuery.andWhere(`city='${city}'`);
    if (x1 && x2 && y2 && y1) {
      subQuery.andWhere(
        `ST_Within(marker::geometry, ST_GeomFromText('POLYGON((${x1} ${y1}, ${x1} ${y2}, ${x2} ${y2}, ${x2} ${y1}, ${x1} ${y1}))'))`,
      );
    }
    //   return subQuery;
    // }, 'sq')
    // .groupBy('cl_id, level')
    console.log(subQuery.getQuery());

    const data = await subQuery.execute();
    return data;
  }

  async findOne(id: number) {
    const driveTest = await this.driveTestRepo.findOne({ where: { id } });
    return driveTest;
  }

  async update(id: number, updateDriveTestDto: UpdateDriveTestDto) {
    const test = await this.driveTestRepo.findOne({ where: { id } });
    await this.driveTestRepo.save({ ...test, ...updateDriveTestDto });
    return this.driveTestRepo.findOne({ where: { id } });
  }

  async remove(id: number) {
    return this.driveTestRepo.remove(
      await this.driveTestRepo.findOneOrFail({ where: { id } }),
    );
  }
}

import { Module } from '@nestjs/common';
import { DriveTestService } from './drive-test.service';
import { DriveTestController } from './drive-test.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DriveTest } from './entities/drive-test.entity';

@Module({
  imports: [TypeOrmModule.forFeature([DriveTest])],
  controllers: [DriveTestController],
  providers: [DriveTestService],
})
export class DriveTestModule {}

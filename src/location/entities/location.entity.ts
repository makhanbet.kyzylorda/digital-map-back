import { Column, Entity, Point, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Location {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({ nullable: true })
  location?: string;

  @Column({ nullable: true })
  district?: string;

  @Column()
  population: number;

  @Column({ type: 'jsonb' })
  technologies: ProviderTechnologies[];

  @Column({
    type: 'point',
  })
  marker: Point;

  @Column({ nullable: true })
  measurement: string;

  @Column({ type: 'jsonb' })
  details: LocationDetails[];
}

export type ProviderCode =
  | 'kcell'
  | 'beeline'
  | 'transtelecom'
  | 'tele2'
  | 'telecom';
export type ProviderTechnologies = '2g' | '3g' | '4g' | '5g' | 'adsl' | 'vols';

export type LocationDetails = {
  providerCode: ProviderCode;
  technologies: ProviderTechnologies[];
  speed: string;
  internet: string;
};

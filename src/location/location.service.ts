import { Injectable } from '@nestjs/common';
import { CreateLocationDto } from './dto/create-location.dto';
import { UpdateLocationDto } from './dto/update-location.dto';
import { FindOptionsWhere, Repository } from 'typeorm';
import { Location } from './entities/location.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { PageOptionsDto } from 'pagination/dtos/page-options.dto';
import { PageDto } from 'pagination/dtos/page.dto';
import { PageMetaDto } from 'pagination/dtos/page-meta.dto';
import { FilterLocationDto } from 'location/dto/filter-location.dto';

@Injectable()
export class LocationService {
  constructor(
    @InjectRepository(Location)
    private readonly locationRepo: Repository<Location>,
  ) {}

  async create(createLocationDto: CreateLocationDto) {
    const location = await this.locationRepo.create(createLocationDto);
    const saved = await this.locationRepo.save(location);
    const find = await this.locationRepo.find({ where: { id: saved.id } });
    return find;
  }

  public async findAllPaginated(
    pageOptionsDto: PageOptionsDto,
  ): Promise<PageDto<Location>> {
    const queryBuilder = this.locationRepo.createQueryBuilder('loc');

    queryBuilder
      .orderBy('loc.id', pageOptionsDto.order)
      .skip(pageOptionsDto.skip)
      .take(pageOptionsDto.take);

    const itemCount = await queryBuilder.getCount();
    const { entities } = await queryBuilder.getRawAndEntities();

    const pageMetaDto = new PageMetaDto({ itemCount, pageOptionsDto });

    return new PageDto(entities, pageMetaDto);
  }

  async findAll({ operator }: FilterLocationDto) {
    const where: FindOptionsWhere<Location> = {};
    // const locations = await this.locationRepo.find({ where });

    const locationsQuery = await this.locationRepo
      .createQueryBuilder('location')
      .select([
        'id',
        "CONCAT(name, ' ',district, ' ', location) as name",
        'population',
        'marker',
        'details',
        'technologies',
        'measurement',
      ]);

    if (operator) {
      locationsQuery.andWhere(
        `location.id IN (SELECT  t.id FROM "location" t, json_array_elements(details::json) d where d ->> 'providerCode' = '${operator}')`,
      );
    }

    console.log(locationsQuery.getQuery());
    const locations = await locationsQuery.execute();
    return locations;
  }

  async update(id: number, updateLocationDto: UpdateLocationDto) {
    const location = await this.locationRepo.findOne({ where: { id } });
    await this.locationRepo.save({ ...location, ...updateLocationDto });
    return this.locationRepo.findOne({ where: { id } });
  }

  async remove(id: number) {
    return this.locationRepo.remove(
      await this.locationRepo.findOneOrFail({ where: { id } }),
    );
  }
}

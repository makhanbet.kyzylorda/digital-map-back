import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { LocationService } from './location.service';
import { CreateLocationDto } from './dto/create-location.dto';
import { UpdateLocationDto } from './dto/update-location.dto';
import { ApiTags } from '@nestjs/swagger';
import { ApiPaginatedResponse } from 'pagination/decorators/api-paginated-response.decorator';
import { PageOptionsDto } from 'pagination/dtos/page-options.dto';
import { PageDto } from 'pagination/dtos/page.dto';
import { Location } from 'location/entities/location.entity';
import { FilterLocationDto } from 'location/dto/filter-location.dto';

@ApiTags('Location')
@Controller('location')
export class LocationController {
  constructor(private readonly locationService: LocationService) {}

  @Post()
  create(@Body() createLocationDto: CreateLocationDto) {
    return this.locationService.create(createLocationDto);
  }

  @Get()
  findAll(@Query() query: FilterLocationDto) {
    return this.locationService.findAll(query);
  }

  @Get('/paginated')
  @ApiPaginatedResponse(Location)
  findAllPaginated(@Query() pageOptionsDto: PageOptionsDto): Promise<PageDto<Location>>  {
    return this.locationService.findAllPaginated(pageOptionsDto);
  }


  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateLocationDto: UpdateLocationDto,
  ) {
    return this.locationService.update(+id, updateLocationDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.locationService.remove(+id);
  }
}

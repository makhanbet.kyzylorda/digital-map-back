import {
  IsArray,
  IsJSON,
  IsNumber,
  IsOptional,
  IsPositive,
  IsString,
} from 'class-validator';
import {
  LocationDetails,
  ProviderTechnologies,
} from 'location/entities/location.entity';
import { Point } from 'typeorm';

export class CreateLocationDto {
  @IsString()
  name: string;

  @IsString()
  @IsOptional()
  location: string;

  @IsString()
  @IsOptional()
  district: string;

  @IsNumber()
  @IsPositive()
  population: number;

  @IsArray()
  technologies: ProviderTechnologies[];

  @IsString()
  marker: Point;

  @IsJSON()
  details: LocationDetails[];

  @IsString()
  @IsOptional()
  measurement: string;
}

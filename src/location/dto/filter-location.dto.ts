import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';
import { ProviderCode } from 'location/entities/location.entity';

export class FilterLocationDto {
  @ApiPropertyOptional()
  @IsOptional()
  operator: ProviderCode;
}

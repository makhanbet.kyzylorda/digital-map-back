import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, FindOneOptions, UpdateResult } from 'typeorm';

import { User } from './entities/user.entity';
import { UpdateUserDto } from './dto/update-user.dto';
import { PageOptionsDto } from 'pagination/dtos/page-options.dto';
import { PageDto } from 'pagination/dtos/page.dto';
import { PageMetaDto } from 'pagination/dtos/page-meta.dto';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {}

  async create(data: Partial<User>): Promise<User> {
    const user = this.userRepository.create(data);
    return this.userRepository.save(user);
  }

  async findOne(where: FindOneOptions<User>): Promise<User> {
    const user = await this.userRepository.findOne(where);

    if (!user) {
      throw new NotFoundException(
        `There isn't any user with identifier: ${where}`,
      );
    }

    return user;
  }

  async findAll(where: FindOneOptions<User>): Promise<User[]> {
    const users = await this.userRepository.find(where);

    return users;
  }

  public async findAllPaginated(
    pageOptionsDto: PageOptionsDto,
  ): Promise<PageDto<User>> {
    const queryBuilder = this.userRepository.createQueryBuilder("u");

    queryBuilder
      .orderBy("u.createdAt", pageOptionsDto.order)
      .skip(pageOptionsDto.skip)
      .take(pageOptionsDto.take);

    const itemCount = await queryBuilder.getCount();
    const { entities } = await queryBuilder.getRawAndEntities();

    const pageMetaDto = new PageMetaDto({ itemCount, pageOptionsDto });

    return new PageDto(entities, pageMetaDto);
  }

  async updateUser(
    userId: number,
    updateUser: Partial<User>,
  ): Promise<Partial<User>> {
    const user = await this.userRepository.findOne({ where: { id: userId } });
    const toSaveUser = this.userRepository.create({
      ...user,
      ...updateUser,
    });
    toSaveUser.hashPassword()
    return await this.userRepository.save(toSaveUser);
  }

  async remove(id: number) {
    return this.userRepository.remove(
      await this.userRepository.findOneOrFail({ where: { id } }),
    );
  }
}

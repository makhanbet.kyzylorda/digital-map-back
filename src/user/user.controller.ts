import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Query,
  UseGuards,
} from '@nestjs/common';

import { AuthUser } from './decorator/user.decorator';
import { User, UserGroups } from './entities/user.entity';
import { UserService } from './user.service';
import { JWTAuthGuard } from 'auth/guards/jwt-auth.guard';
import { SessionAuthGuard } from 'auth/guards/session-auth.guard';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { Groups } from './decorator/group.decorator';
import { UpdateUserDto } from 'user/dto/update-user.dto';
import { UpdateResult } from 'typeorm';
import { ApiPaginatedResponse } from 'pagination/decorators/api-paginated-response.decorator';
import { PlanVols } from 'plan-vols/entities/plan-vols.entity';
import { PageOptionsDto } from 'pagination/dtos/page-options.dto';
import { PageDto } from 'pagination/dtos/page.dto';

@ApiTags('User')
@Controller('user')
// @UseGuards(JWTAuthGuard)
// @UseGuards(SessionAuthGuard)
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get('/')
  async me(@AuthUser() user: User): Promise<User> {
    return user;
  }

  @Get('/all')
  // @Groups(UserGroups.ADMIN)
  async all(): Promise<User[]> {
    const users = await this.userService.findAll({});
    return users;
  }

  @Get('/paginated')
  @ApiPaginatedResponse(PlanVols)
  findAllPaginated(@Query() pageOptionsDto: PageOptionsDto): Promise<PageDto<User>>  {
    return this.userService.findAllPaginated(pageOptionsDto);
  }

  @Patch('/:id')
  // @Groups(UserGroups.ADMIN)
  async update(
    @Param('id') id: string,
    @Body() updateUser: UpdateUserDto,
  ): Promise<Partial<User>> {
    return await this.userService.updateUser(+id, updateUser);
  }

  @Delete(':id')
  // @Groups(UserGroups.ADMIN)
  remove(@Param('id') id: string) {
    return this.userService.remove(+id);
  }
}

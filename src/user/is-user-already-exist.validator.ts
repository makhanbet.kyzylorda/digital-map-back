import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import {
  ValidationArguments,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { Repository } from 'typeorm';
import { User } from './entities/user.entity';
import { UserService } from './user.service';
import { IsUserAlreadyExistsInterface } from './decorator/is-user-already-exist.decorator';

@ValidatorConstraint({ name: 'isUserAlreadyExist', async: true })
@Injectable()
export class IsUserAlreadyExist implements ValidatorConstraintInterface {
  constructor(private readonly userService: UserService) {}

  async validate(value: string, args?: ValidationArguments): Promise<boolean> {
    const { column }: IsUserAlreadyExistsInterface = args.constraints[0];
    let user;
    try {
      user = await this.userService.findOne({
        where: { [column]: value, isActive: true },
      });
    } catch (err) {}

    return user === null || user === undefined;
  }

  defaultMessage(): string {
    return 'The user «$value» is already register.';
  }
}

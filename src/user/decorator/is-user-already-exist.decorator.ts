import { ValidationOptions, registerDecorator } from 'class-validator';
import { IsUserAlreadyExist } from 'user/is-user-already-exist.validator';

export type IsUserAlreadyExistsInterface = { column: string };

export function isUserAlreadyExist(
  options: IsUserAlreadyExistsInterface,
  validationOptions?: ValidationOptions,
) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: 'isUserAlreadyExist',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      constraints: [options],
      validator: IsUserAlreadyExist,
    });
  };
}

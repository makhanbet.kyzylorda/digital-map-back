import { SetMetadata } from "@nestjs/common";
import { UserGroups } from "../entities/user.entity";

export const Groups = (...groups: UserGroups[]) => SetMetadata("userGroup", groups);

import { ApiProperty, OmitType, PartialType } from '@nestjs/swagger';
import { SignUp } from 'auth/dto/sign-up.dto';
import { IsBoolean, IsOptional } from 'class-validator';

export class UpdateUserDto extends PartialType(OmitType(SignUp, ['password'])) {
  @ApiProperty()
  @IsBoolean()
  @IsOptional()
  isActive: boolean;
}
